<div class="container py-3">
    <div class="row flex-wrap my-4 align-items-end">
        <form method="POST" action="" class="justify-content-evenly">
            <div class="col-3 d-flex flex-wrap flex-column justify-content-center px-2">
                <label>Promotions</label>
                <select name="promotions_name" class="form-control select-promotion">
                    <option value="0">Toutes</option>
                    <?php
                    foreach ($promotionsList as $promotionList) : ?>
                        <option name="promotions_name" value="<?= $promotionList->id; ?>"><?= $promotionList->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-3 d-flex flex-wrap justify-content-center px-2">
                <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="search">
            </div>
            <div class="col-3 d-flex flex-wrap justify-content-center mx-2">
                <?php
                foreach ($competencesList as $competenceList) : ?>
                    <div class="form-check mx-2">
                        <input class="form-check-input" type="checkbox" id="flexCheckDefault" name="competences_name[]" value="<?= $competenceList->id; ?>" checked>
                        <label class="form-check-label" for="flexCheckDefault"><?= $competenceList->name; ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-2 d-flex flex-wrap justify-content-center px-2">
                <button type="submit" class="btn btn-secondary">Valider</button>
            </div>
        </form>
    </div>
    <div class="row results-list">
        <?php
        foreach ($results as $result) : ?>
            <div class="col cards-container d-flex justify-content-evenly">
                <div class="card-one">
                    <header>
                        <div class="avatar">
                            <img src="<?= $result->image; ?>" alt="photo apprenant" />
                        </div>
                    </header>

                    <h3><strong><?= $result->nom; ?></strong> <?= $result->prenom; ?></h3>
                    <div class="desc">
                        <p><?= $result->excerpt->rendered; ?></p>
                    </div>
                    <div class="promotion">
                        <p class="promotion-bordure"><?= $result->promotion->name; ?></p>
                    </div>
                    <div class="skills row">
                        <?php $competences = $result->competences;
                        foreach ($competences as $competence) : ?>
                            <p class="col"><?= $competence->name; ?></p>
                        <?php endforeach; ?>
                    </div>

                    <footer>
                        <a href="<?= $result->linkedin ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="<?= $result->portfolio ?>" target="_blank"><i class="fas fa-address-book"></i></a>
                        <a href="<?= $result->cv ?>" target="_blank"><i class="fas fa-file-download"></i></a>
                    </footer>
                </div>
            </div>
        <?php endforeach; ?>
    </div>