<?php

class Requete
{
    public $apiUrl;

    public function dataApi($apiUrl)
    {
        $this->apiUrl = $apiUrl;
        $data = $this->fetchData();
        return $this->decode($data);
    }

    private function fetchData()
    {
        $ch = curl_init($this->apiUrl);
        // Set URL and header options
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);

        // Excecuting the curl call
        $data = curl_exec($ch);

        // Get the srtout content and clean the buffer
        if (!$data) {
            throw new Error("Erreur");
            // Close the Curl resource
            curl_close($ch);
        }
        return $data;
        // Close the Curl resource
        curl_close($ch);
    }

    private function decode($jsonstring)
    {
        return json_decode($jsonstring);
    }
}
