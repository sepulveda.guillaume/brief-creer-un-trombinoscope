<?php
class ResultController
{
    private $templateEngine;
    private $model;

    public function __construct($templateEngine, $model)
    {
        $this->templateEngine = $templateEngine;
        $this->model = $model;
    }

    public function displayList()
    {
        if (isset($_POST['promotions_name']) && isset($_POST['competences_name']) && isset($_POST['search'])) {
            $id_list = $_POST['promotions_name'];

            $url = "";
            $competences_list = $_POST['competences_name'];
            foreach ($competences_list as $competence_list) {
                $url .= $competence_list . ',';
            }

            if ($id_list === '0') {
                $results = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100&Competence=" . $url);
            } else {
                $results = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100&Competence=" . $url . "&Promotion=" . $id_list);
            }

            $searchResult = urlencode($_POST['search']);
            if ($searchResult !== "") {
                $results = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100&search=" . $searchResult);
            }
        } else {
            $results = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100");
        }
        $promotionsList = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/promotion");
        $competencesList = $this->model->dataApi("https://www.lecoledunumerique.fr/wp-json/wp/v2/competence");
        return $this->templateEngine->render('template.php', array('results' => $results, 'promotionsList' => $promotionsList, 'competencesList' => $competencesList));
    }
}
