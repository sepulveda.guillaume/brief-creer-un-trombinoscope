<?php

class TemplateEngine
{
    /**
     * @var string
     */
    private $templateDir;

    public function __construct($templateDir)
    {
        $this->templateDir = $templateDir;
    }

    /**
     * {@inheritDoc}
     */
    public function render($template, array $parameters = array())
    {
        extract($parameters); // vérifie chaque clé afin de contrôler si elle a un nom de variable valide.

        if (false === $this->isAbsolutePath($template)) {
            $template = $this->templateDir . DIRECTORY_SEPARATOR . $template;
        }

        ob_start();
        include $template;

        return ob_get_clean();
    }

    private function isAbsolutePath($file)
    {
        if (strspn($file, '/\\', 0, 1) // Trouve la longueur du segment initial d'une chaîne contenant tous les caractères d'un masque donné
            || (strlen($file) > 3 && ctype_alpha($file[0]) // Vérifie qu'une chaîne est alphabétique
            && substr($file, 1, 1) === ':' // Retourne un segment de chaîne
            && (strspn($file, '/\\', 2, 1)) // Trouve un segment de chaîne ne contenant pas certains caractères
        )
        || null !== parse_url($file, PHP_URL_SCHEME) // Analyse une URL et retourne ses composants
        ) {
            return true;
        }

        return false;
    }
}