<?php

require_once 'controller/results.php';
require_once 'app/TemplateEngine.php';
require_once 'model/Requete.php';

$engine = new TemplateEngine('view');

$request = new Requete();

$controller = new ResultController($engine, $request);

require_once 'view/header.php';
echo $controller->displayList();
require_once 'view/footer.php';
